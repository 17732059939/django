from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from adm.models import Navigation


def index(request):
    nav_list = Navigation.objects.filter(n_status="显示").order_by('n_sort')
    context = {
        'nav_list': nav_list
    }
    return render(request, 'home/index/index.html', context=context)