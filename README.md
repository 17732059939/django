

    """

         user_register_forms = UserRegisterForms(require_POST)
         if user_register_forms.is_valid():
             email = user_register_forms.cleaned_data['email']
             password = user_register_forms.cleaned_data['password']

             userlist = UserProfile.objects.filter(Q(name=email) | Q(email=email))
             if userlist:
                 return render(request, 'home/user/register.html',{
                     'msg': "用户已经存在"
                 })
             else:
                 a = UserProfile()
                 a.email = email
                 a.password = password
                 a.name = email
                 a.save()
                 return redirect(reverse('adm:index'))

         else:
             return render(request, 'home/user/register.html', {
                 'user_register_forms': user_register_forms
             })

 """




# Django

#### 介绍
Django

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
