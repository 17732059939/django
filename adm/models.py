from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.

class Navigation(models.Model):
    n_title = models.CharField(max_length=20, null=False)
    # n_status = models.IntegerField(default=1)
    n_status = models.CharField(max_length=10, default='显示')
    n_sort = models.IntegerField(default=100)
    n_url = models.CharField(max_length=50, default='')


# 商品分类模型
class Cates(models.Model):
    name = models.CharField(max_length=20)
    pid = models.IntegerField(default=0)
    path = models.CharField(max_length=50)
    status = models.CharField(max_length=10, default='显示')


# 菜单管理
class Menu(models.Model):
    name = models.CharField(max_length=20, unique=True, verbose_name='名称')
    pid = models.IntegerField(default=0, verbose_name='父级')
    path = models.CharField(max_length=50, verbose_name='路径')
    icon = models.CharField(max_length=100, default='&#xe62b;', verbose_name='图标')
    url = models.CharField(max_length=100, null=True, verbose_name='访问路由')
    status = models.CharField(max_length=10, choices=(('open', '开启'), ('close', '关闭')), default='open', verbose_name='状态')

    def __str__(self):
        return self.m_name

    class Meta:
        verbose_name = '系统菜单'
        verbose_name_plural = verbose_name
        db_table = 'adm_menu'



class UserProfile(models.Model):
    name = models.CharField(max_length=20, default='', verbose_name='姓名')
    email = models.CharField(max_length=50, default='', verbose_name='邮箱')
    password = models.CharField(max_length=100, default='', verbose_name='密码')
    birthday = models.DateField(null=True, blank=True, verbose_name='出生日前')
    gender = models.CharField(max_length=10, choices=(('male', '男'), ('female', '女')), default='male', verbose_name='性别')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '用户信息'
        verbose_name_plural = verbose_name
        db_table = 'adm_userprofile'
        ordering = ['id']

