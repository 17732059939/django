
from django import forms

class UserRegisterForms(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True, max_length=15, min_length=3, error_messages={
        "request": '密码必须填写',
        "max_length": "密码最大长度为15",
        "min_length": "密码最小3位"
    })

