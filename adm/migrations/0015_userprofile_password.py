# Generated by Django 3.0.3 on 2020-02-25 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adm', '0014_userprofile'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='password',
            field=models.CharField(default='', max_length=100, verbose_name='密码'),
        ),
    ]
