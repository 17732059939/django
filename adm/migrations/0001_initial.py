# Generated by Django 3.0.3 on 2020-02-22 17:04

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Navigation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('n_title', models.CharField(max_length=20)),
                ('n_status', models.IntegerField(default=1)),
                ('n_sort', models.IntegerField(default=100)),
            ],
        ),
    ]
