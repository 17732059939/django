# Generated by Django 3.0.3 on 2020-02-25 11:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adm', '0008_auto_20200225_1117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menu',
            name='path',
            field=models.CharField(default='&#xe62b;', max_length=50, verbose_name='路径'),
        ),
    ]
