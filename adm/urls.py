from django.urls import re_path

from . import views
app_name = 'adm'
urlpatterns = [
    re_path(r"^index/", views.index, name='index'),

    # 导航测试
    re_path(r"^navigation/", views.navigation, name='navigation'),
    re_path(r"^navigation_add/", views.navigation_add, name='navigation_add'),
    re_path(r"^get_navigation_list/", views.get_navigaton_list, name='get_navigation_list'),
    re_path(r'^navigation_edit/', views.navigation_edit, name='navigation_edit'),
    re_path(r'^navigation_del/$', views.navigation_del, name='navigation_del'),

    # 无限分类测试
    re_path(r'^cate/', views.cate, name='cate'),
    re_path(r"^cate_add/", views.cate_add, name='cate_add'),
    re_path(r"^cate_edit/", views.cate_edit, name='cate_edit'),
    re_path(r"^cate_del/", views.cate_del, name='cate_del'),


    # 菜单开发
    re_path(r'^menu/', views.menu, name='menu'),
    re_path(r'^menu_add/', views.menu_add, name='menu_add'),
    re_path(r'^menu_list/', views.menu_list, name='menu_list'),


    # 登陆注册
    re_path(r'^user_register/$', views.user_register, name='user_register'),
    re_path(r'^user_register_post/$', views.user_register_post, name='user_register_post'),



    re_path("setting/", views.setting, name='setting'),

]