import time
from _md5 import md5
from audioop import reverse

from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse, JsonResponse, response
from django.shortcuts import render, redirect

# Create your views here.
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from adm import models
from adm.models import Navigation, Cates, Menu, UserProfile
from adm.userfroms import UserRegisterForms
from lib import Helper
from lib.HttpCode import result

import json
from django.core import serializers
def index(request):
    return render(request, 'adm/index/index.html')


@xframe_options_exempt
def navigation(request):
    return render(request, 'adm/navigation/index.html')


@xframe_options_exempt
@csrf_exempt
def navigation_add(request):
    if request.method == 'GET':
        return render(request, 'adm/navigation/navigation_add.html')
    else:


        title = request.POST.get('title')
        print(title)
        sort = request.POST.get('sort')
        status = request.POST.get('status')
        url = request.POST.get('url')
        try:
            navigation = Navigation()
            navigation.n_title = title
            navigation.n_url = url
            if sort:
                navigation.n_sort = sort
            navigation.n_status = status
            navigation.save()
            data = {
                'code': 200,
                'msg': '添加成功'
            }
            return JsonResponse(data)
        except:
            data = {
                'code': 201,
                'msg': '添加失败'
            }
            return JsonResponse(data)





def setting(request):
    return render(request, 'adm/setting/index.html')


def get_navigaton_list(request):
    data = {}
    json_data =[]
    nav_list = Navigation.objects.all()
    for nav in nav_list:
        data["id"] = nav.id
        data["title"] = nav.n_title
        json_data.append(data)

    return result(0, "ok", data=json.dumps(json_data))





@xframe_options_exempt
def get_navigaton_list(request):
    datas = models.Navigation.objects.all()
    dataCount = datas.count()
    req = request.POST
    list = []  # 存放所有的数据列表
    for data in datas:
        dict = {}  # 存放数据的字典
        dict["id"] = data.id
        dict["title"] = data.n_title
        dict["status"] = data.n_status
        dict["sort"] = data.n_sort
        dict["url"] = data.n_url
        list.append(dict)
    # print("列表",list)
    pageIndex = request.GET.get('page')  # pageIndex = request.POST.get('pageIndex')
    pageSize = request.GET.get('limit')  # pageSize = request.POST.get('pageSize')
    pageInator = Paginator(list, pageSize)
    contacts = pageInator.page(pageIndex)
    res = []  # 最终返回的结果集合
    for contact in contacts:
        # print(contact)
        res.append(contact)
    Result = {"code": 0, "Msg": "成功", "count": dataCount, "data": res}
    return JsonResponse(Result)


@xframe_options_exempt
@csrf_exempt
def navigation_edit(request):
    if request.method == 'GET':
        n_id = int(request.GET.get('id'))
        navigations = models.Navigation.objects.filter(id=n_id)
        for navigation in navigations:
            data = {
                'id': navigation.id,
                'title': navigation.n_title,
                'status': navigation.n_status,
                'sort': navigation.n_sort,
                'url': navigation.n_url
            }
        return render(request, 'adm/navigation/navigation_edit.html', context=data)
    else:
        title = request.POST.get('title')
        sort = request.POST.get('sort')
        status = request.POST.get('status')
        url = request.POST.get('url')
        n_id = request.POST.get('id')

        try:
            navigation = Navigation()
            navigation.n_title = title
            navigation.n_url = url
            if sort:
                navigation.n_sort = sort
            navigation.n_status = status
            navigation.id = n_id
            navigation.save()
            data = {
                'code': 200,
                'msg': '修改成功'
            }
            return JsonResponse(data)
        except:
            data = {
                'code': 201,
                'msg': '修改失败'
            }
            return JsonResponse(data)


@require_POST
@csrf_exempt
def navigation_del(request):
    n_id = request.POST['n_id']
    try:
        navigation = Navigation.objects.get(id=n_id)
        navigation.delete()
        data = {
            'code': 200,
            'msg': '删除成功'
        }
        return JsonResponse(data)
    except:
        data = {
            'code': 201,
            'msg': '删除失败'
        }
        return JsonResponse(data)


def navigation_add_post(request):
    return None


@xframe_options_exempt
def cate(request):
    catelist = get_cates_all(Cates)
    return render(request, 'adm/cate/index.html', context={'catelist': catelist})


@xframe_options_exempt
@csrf_exempt
def cate_add(request):
    if request.method == 'GET':

        catelist = get_cates_all(Cates)
        # 分配数据
        return render(request, 'adm/cate/cate_add.html', context={'catelist': catelist})
    else:
        c_name = request.POST.get('name')
        c_status = request.POST.get('status')

        c_pid = int(request.POST.get('pid'))

        if c_pid == 0:
            c_path = '0,'
        else:
            pob = Cates.objects.get(id=c_pid)
            c_path = str(pob.path) + str(c_pid) + ','
        try:
            cates = Cates()
            cates.name = c_name
            cates.pid = c_pid
            cates.status = c_status
            cates.path = c_path
            cates.save()
            data = {
                'code': 200,
                'msg': '添加成功'
            }
            return JsonResponse(data)
        except:
            data = {
                'code': 201,
                'msg': '添加失败'
            }
            return JsonResponse(data)



# 封装函数.格式化获取分类数据
def get_cates_all(_model):
    # 获取所有的分类
    # data = Cates.objects.all()
    # select *,concat(path,id) as paths from myadmin_cates order by paths;
    model = _model
    data = model.objects.extra(select={'paths': 'concat(path,id)'}).order_by('paths')
    # 处理数据的顺序,缩进,及父级名
    for x in data:
        l = x.path.count(',')-1  # 0,1,2,
        x.sub = l*'|----'
        if x.pid == 0:
            x.pname = '顶级分类'
        else:
            pob = model.objects.get(id=x.pid)
            x.pname = pob.name
        # print(x.name,l)
    return data

@require_POST
@csrf_exempt
def cate_edit(request):
    c_name = request.POST.get('name')
    c_id = int(request.POST.get('id'))
    try:
        models.Cates.objects.filter(id=c_id).update(name=c_name)
        data = {
            'code': 200,
            'msg': '添加成功'
        }
        return JsonResponse(data)
    except:
        data = {
            'code': 201,
            'msg': '添加失败'
        }
        return JsonResponse(data)


@require_POST
@csrf_exempt
def cate_del(request):
    c_id = request.POST['cid']
    try:
        # navigation = Navigation.objects.get(id=n_id)
        # navigation.delete()
        data = {
            'code': 200,
            'msg': '删除成功'
        }
        return JsonResponse(data)
    except:
        data = {
            'code': 201,
            'msg': '删除失败'
        }
        return JsonResponse(data)




@xframe_options_exempt
@csrf_exempt
def menu(request):
    """
    # 菜单
    """
    # 分配数据
    return render(request, 'adm/menu/index.html')


@xframe_options_exempt
@csrf_exempt
def menu_add(request):
    if request.method == 'GET':

        catelist = get_cates_all(Menu)
        # 分配数据
        return render(request, 'adm/menu/menu_add.html', context={'catelist': catelist})
    else:
        m_name = request.POST.get('name')
        if m_name:
            m_status = request.POST.get('status')
            m_pid = int(request.POST.get('pid'))
            m_url = request.POST.get('url')
            m_icon = request.POST.get('icon')

            if m_pid == 0:
                m_path = '0,'
            else:
                pob = Menu.objects.get(id=m_pid)
                m_path = str(pob.path) + str(m_pid) + ','

            try:

                menu = Menu()
                menu.name = m_name
                menu.pid = m_pid
                menu.status = m_status
                menu.path = m_path
                menu.icon = m_icon
                menu.m_url = m_url
                menu.save()

                data = {
                    'code': 200,
                    'msg': '添加成功'
                }
                return JsonResponse(data)
            except:
                data = {
                    'code': 201,
                    'msg': '添加失败'
                }
                return JsonResponse(data)
        data = {
            'code': 201,
            'msg': '提交的信息有误'
        }
        return JsonResponse(data)


@xframe_options_exempt
@csrf_exempt
def menu_list(request):
    data = {}
    province = serializers.serialize("json", Menu.objects.all())
    data["data"] = json.loads(province)


    return JsonResponse(data, safe=False)




@xframe_options_exempt
@csrf_exempt
def user_register(request):
        return render(request, 'home/user/register.html')




@csrf_exempt
def user_register_post(request):

    user_register_forms = UserRegisterForms(request.POST)
    print(user_register_forms)
    if user_register_forms.is_valid():
        email = user_register_forms.cleaned_data['email']
        password = user_register_forms.cleaned_data['password']



        userlist = UserProfile.objects.filter(Q(name=email) | Q(email=email))

        if userlist:
            return render(request, 'home/user/register.html', {
                'msg': "用户已经存在"
            })

    else:
        return render(request, 'home/user/register.html', {
            'user_register_forms': "user_register_forms"
        })

